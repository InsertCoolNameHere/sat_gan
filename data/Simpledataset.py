import glob
import random
import os
import numpy as np

import torch
from torch.utils.data import Dataset
from PIL import Image
import torchvision.transforms as transforms
from torchvision.utils import save_image, make_grid
import torch.nn as nn

import matplotlib.pyplot as plt

# Normalization parameters for pre-trained PyTorch models
mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])

#HR to LR RATIO
resolution_factor = 4

class SimpleDataset(Dataset):
    def __init__(self, root, hr_shape, ext):
        hr_height, hr_width = hr_shape

        # Transforms for low resolution images and high resolution images
        self.lr_transform = transforms.Compose(
            [
                transforms.Resize((hr_height // resolution_factor, hr_width // resolution_factor), Image.BICUBIC),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
        self.hr_transform = transforms.Compose(
            [
                transforms.Resize((hr_height, hr_width), Image.BICUBIC),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
        self.viewTransform = transforms.ToPILImage(mode='RGB')

        self.files = sorted(glob.glob(root + "/*"+ext))
        #print(self.files)

    def __getitem__(self, index):
        img = Image.open(self.files[index % len(self.files)])
        img_lr = self.lr_transform(img)
        img_hr = self.hr_transform(img)

        return {"lr": img_lr, "hr": img_hr}

    def __len__(self):
        return len(self.files)


    # DENORMALIZE A NORMALIZED IMAGE TENSOR
    def denormalize(self, img):

        std_inv = 1 / (std + 1e-7)
        mean_inv = -mean * std_inv

        std_inv = torch.as_tensor(std_inv)
        mean_inv = torch.as_tensor(mean_inv)
        invTrans = transforms.Compose([transforms.Normalize(mean=mean_inv,std=std_inv)])

        '''invTrans = transforms.Compose([transforms.Normalize(mean=[0., 0., 0.],
                                                            std=[1 / 0.229, 1 / 0.224, 1 / 0.225]),
                                       transforms.Normalize(mean=[-0.485, -0.456, -0.406],
                                                            std=[1., 1., 1.]),
                                       ])'''

        return invTrans(img)

    # TRANSFORMS AN IMAGE TENSOR TO A SHOWABLE FORM
    def showableImage(self, img):
        img = self.denormalize(img)
        return self.viewTransform(img.squeeze())


if __name__ == "__main__":

    #print(torch.multiprocessing.get_all_sharing_strategies())
    ds = SimpleDataset('/s/chopin/b/grad/sapmitra/Documents/superResolution/bsd/images/train', (256,256), '.jpg')
    imgs = ds.__getitem__(0)

    imgs_lr = imgs["lr"]
    imgs_hr = imgs["hr"]

    #imgs_lr = ds.denormalize(imgs_lr)
    #imgs_hr = ds.denormalize(imgs_hr)

    #print(imgs_lr.size())


    #imgs_lr = make_grid(imgs_lr, nrow=1, normalize=True)
    #imgs_hr = make_grid(imgs_hr, nrow=1, normalize=True)
    #img_grid = torch.cat((imgs_lr, imgs_hr), -1)

    #print(type(imgs_lr))

    #f, axarr = plt.subplots(2, 1)

    # use the created array to output your multiple images. In this case I have stacked 4 images vertically
    #axarr[0].imshow(imgs_lr[1,:])
    #axarr[1].imshow(imgs_hr[1,:])

    #plt.show()

    #trans = transforms.ToPILImage(mode='RGB')
    '''imgs_lr = ds.showableImage(imgs_lr)
    plt.imshow(imgs_lr)
    plt.show()'''

    imgs_lr = ds.denormalize(imgs_lr)
    imgs_lr = make_grid(imgs_lr, nrow=1, normalize=False)
    img_grid = torch.cat((imgs_lr,), -1)
    save_image(img_grid, "/s/chopin/b/grad/sapmitra/Documents/superResolution/bsd/images/saved_images/riki.png" , normalize=False)
