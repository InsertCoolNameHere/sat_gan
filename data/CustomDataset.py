#LOADING THE IMAGE FILES IN A CUSTOM WAY
from __future__ import print_function, division
import os
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

from PIL import Image

#IMAGES WILL BE pizel_resxpixel_res
pixel_res = 32

class CustomDataset(Dataset):

    # READS ALL IMAGE FILE IN THE TARGET DIRECTORY AND CREATES TWO LISTS - KEYS & FILEPATHS WHOSE INDICES CORRESPOND
    def constructImageDictionary(self, img_dir, ext):
        img_keys = []
        img_paths = []
        for roots, dir, files in os.walk(img_dir):
            for file in files:
                if ext in file:
                    file_abs_path = os.path.join(roots, file)
                    tokens = file_abs_path.split('/');
                    ln = len(tokens)
                    geohash = tokens[ln - 2]

                    f_tokens = file.split('$')
                    date = f_tokens[0]

                    img_keys.append(geohash + '$' + date)
                    img_paths.append(file_abs_path)
        return img_keys, img_paths

    # IMAGE UPSCALING
    # img: PIL image
    # INCREASE RESOLUTION OF IMAGE BY FACTOR OF 2^pow
    def upscaleImage(self, img, pow, method=Image.BICUBIC):
        ow, oh = img.size
        scale = 2**pow
        h = int(round(oh * scale))
        w = int(round(ow * scale))
        return img.resize((w, h), method)

    # IMAGE DOWNGRADING
    # img: PIL image
    # DECREASE RESOLUTION OF IMAGE BY FACTOR OF 2^pow
    def downscaleImage(self, img, pow, method=Image.BICUBIC):
        ow, oh = img.size
        scale = 2**pow
        h = int(round(oh / scale))
        w = int(round(ow / scale))
        return img.resize((w, h), method)

    def imageTransformer(self, image):
        new_image = image.resize((pixel_res, pixel_res))
        return new_image


    def __init__(self, img_dir, img_type, transform=None):

        self.img_dir = img_dir
        self.transform = transform
        self.img_keys, self.img_paths = self.constructImageDictionary(img_dir, img_type)


    # RETURNS A LOW RES AND HIGH RES VERSION OF THE IMAGE AT A GIVEN INDEX
    def __getitem__(self, index):
        key = self.img_keys[index]
        image_path = self.img_paths[index]
        img = Image.open(image_path)

        # ORIGINAL IMAGE
        tr = self.getTransforms()

        hr_image = self.imageTransformer(img)
        lr_image = self.downscaleImage(img, 1)
        return {'key':key,'lr':lr_image,'hr': hr_image}

    def __len__(self):
        return len(self.img_keys)

    # DISPLAY AN IMAGE AT A GIVEN PATH
    def displayImage(self, image_path):
        img = Image.open(image_path)
        plt.figure()
        plt.imshow(img)
        plt.show()

    # DISPLAY AN IMAGE OBJECT
    def displayImageMem(self, img):
        plt.figure()
        plt.imshow(img)
        plt.show()

    def getTransforms(self):
        transform_list = []

        transform_list += [transforms.ToTensor(),
                           transforms.Normalize((0.5, 0.5, 0.5),
                                                (0.5, 0.5, 0.5))]
        return transforms.Compose(transform_list)

if __name__ == "__main__":
    #print(torch.multiprocessing.get_all_sharing_strategies())
    cdl = CustomDataset('/s/chopin/b/grad/sapmitra/Documents/superResolution/cifar/', '.jpg')
    dict = cdl.__getitem__(0)

    # subplot(r,c) provide the no. of rows and columns
    f, axarr = plt.subplots(3, 1)

    # use the created array to output your multiple images. In this case I have stacked 4 images vertically
    axarr[0].imshow(dict['hr'])
    axarr[1].imshow(dict['lr'])
    axarr[2].imshow(cdl.upscaleImage(dict['lr'], 1))

    '''print(type(dict['hr']))
    transform = cdl.getTransforms()
    n1 = transform(dict['hr'])

    print(n1.size())
    plt.show()'''


    '''plt.figure(figsize=(1, 3))
    i1 = plt.figure(1)
    plt.imshow(dict['hr'])
    #i1.show()

    i2 = plt.figure(2)
    plt.imshow(dict['lr'])
    #i2.show()

    i3 = plt.figure(3)
    plt.imshow(cdl.upscaleImage(dict['lr'], 2))
    #i3.show()

    plt.show()'''
