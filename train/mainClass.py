import argparse
import os
import numpy as np
import math
import itertools
import sys

import torchvision.transforms as transforms
from torchvision.utils import save_image, make_grid

from torch.utils.data import DataLoader
from torch.autograd import Variable

from model.srgan_simple import *
from data.CustomDataset import *
from data.Simpledataset import *

import torch.nn as nn
import torch.nn.functional as F
import torch

if __name__ == "__main__":
    baseDir="C:\\Users\\sapmitra\\Documents\\sapmitra\\SuperResolution\\bsd\\images\\"
    os.makedirs("images", exist_ok=True)
    os.makedirs("saved_models", exist_ok=True)

    checkpointPath = "C:\\Users\\sapmitra\\Documents\\sapmitra\\SuperResolution\\checkPoints"

    start_epoch=0
    total_epochs=20
    batch_size=4 #size of the batches 4
    lr=0.001 #adam: learning rate 0.0002
    b1=0.5 #adam: decay of first order momentum of gradient
    b2=0.999  #adam: decay of first order momentum of gradient
    decay_epoch=0 #epoch from which to start lr decay
    n_cpu=8 #number of cpu threads to use during batch generation
    hr_height=256 #high res. image height
    hr_width=256 #high res. image width
    channels=3 #number of image channels
    sample_interval=50 #interval between saving image samples
    checkpoint_interval=-1 #interval between model checkpoints

    cuda = torch.cuda.is_available()
    #cuda=False

    print("GPU AVAILABLE:"+str(cuda))
    hr_shape = (hr_height, hr_width)

    # Initialize generator and discriminator
    generator = GeneratorResNet()
    discriminator = Discriminator(input_shape=(channels, *hr_shape))
    feature_extractor = FeatureExtractor()

    # Set feature extractor to inference mode
    feature_extractor.eval()

    # Losses
    criterion_GAN = torch.nn.MSELoss()
    criterion_content = torch.nn.L1Loss()

    if cuda:
        generator = generator.cuda()
        discriminator = discriminator.cuda()
        feature_extractor = feature_extractor.cuda()
        criterion_GAN = criterion_GAN.cuda()
        criterion_content = criterion_content.cuda()

    if start_epoch != 0:
        # Load pretrained models
        generator.load_state_dict(torch.load(checkpointPath+"/generator_%d.pth"))
        discriminator.load_state_dict(torch.load(checkpointPath+"/discriminator_%d.pth"))

    # ADAM Optimizers FOR TRAINING
    optimizer_G = torch.optim.Adam(generator.parameters(), lr=lr, betas=(b1, b2))
    optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=lr, betas=(b1, b2))

    Tensor = torch.cuda.FloatTensor if cuda else torch.Tensor

    ds = SimpleDataset(baseDir+"/train", (hr_width,hr_height), '.jpg')
    dataloader = DataLoader(
        ds,
        batch_size=batch_size,
        shuffle=True,
        num_workers=n_cpu,
    )

    # ----------
    #  Training
    # ----------

    print("BEGIN TRAINING.....")
    for epoch in range(start_epoch, total_epochs):
        for i, imgs in enumerate(dataloader):

            # Configure model input
            imgs_lr = Variable(imgs["lr"].type(Tensor))
            imgs_hr = Variable(imgs["hr"].type(Tensor))

            # Adversarial ground truths
            #valid = Variable(Tensor(np.ones((imgs_lr.size(0), *discriminator.output_shape))), requires_grad=False)
            #fake = Variable(Tensor(np.zeros((imgs_lr.size(0), *discriminator.output_shape))), requires_grad=False)
            valid = Variable(Tensor(np.ones((imgs_lr.size(0), 1))), requires_grad=False)
            fake = Variable(Tensor(np.zeros((imgs_lr.size(0), 1))), requires_grad=False)
            # ------------------
            #  Train Generators
            # ------------------

            optimizer_G.zero_grad()

            # Generate a high resolution image from low resolution input
            gen_hr = generator(imgs_lr)

            # DISCRIMINATOR'S VERDICT OF FAKE IMAGE
            discrim_verdict = discriminator(gen_hr)

            #print(discrim_verdict.size())
            # Adversarial loss
            loss_GAN = criterion_GAN(discrim_verdict, valid)

            # Content loss
            gen_features = feature_extractor(gen_hr)
            real_features = feature_extractor(imgs_hr)
            loss_content = criterion_content(gen_features, real_features.detach())

            # Total loss
            loss_G = loss_content + 1e-3 * loss_GAN

            loss_G.backward()
            optimizer_G.step()

            # ---------------------
            #  Train Discriminator
            # ---------------------

            optimizer_D.zero_grad()

            # Loss of real and fake images
            loss_real = criterion_GAN(discriminator(imgs_hr), valid)
            loss_fake = criterion_GAN(discriminator(gen_hr.detach()), fake)

            # Total loss
            loss_D = (loss_real + loss_fake) / 2

            loss_D.backward()
            optimizer_D.step()

            # --------------
            #  Log Progress
            # --------------

            print(
                "[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f]"
                % (start_epoch, total_epochs, i, len(dataloader), loss_D.item(), loss_G.item()), flush=True
            )
            #sys.stdout.flush()

            batches_done = epoch * len(dataloader) + i
            if batches_done % sample_interval == 0:
                # Save image grid with upsampled inputs and SRGAN outputs
                imgs_lr = nn.functional.interpolate(imgs_lr, scale_factor=4)

                #gen_color = ds.denormalize(gen_hr)
                gen_color = make_grid(gen_hr, nrow=1, normalize=False)

                gen_hr = make_grid(gen_hr, nrow=1, normalize=True)
                imgs_lr = make_grid(imgs_lr, nrow=1, normalize=True)
                img_grid = torch.cat((imgs_lr, gen_hr, gen_color,), -1)
                save_image(img_grid, baseDir+"saved_images/%d.png" % batches_done, normalize=False)
                print("MADE SAVE %d.png" % batches_done)

        print(
            "[Epoch %d/%d] [D loss: %f] [G loss: %f]"
            % (epoch, total_epochs, loss_D.item(), loss_G.item()), flush=True
        )

        if checkpoint_interval != -1 and epoch % checkpoint_interval == 0:
            # Save model checkpoints
            torch.save(generator.state_dict(), checkpointPath+"/generator_%d.pth" % epoch)
            torch.save(discriminator.state_dict(), baseDir+"/discriminator_%d.pth" % epoch)